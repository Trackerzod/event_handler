# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170425162405) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "browsers", force: :cascade do |t|
    t.string "name"
  end

  create_table "domains", force: :cascade do |t|
    t.string "name"
  end

  create_table "events", force: :cascade do |t|
    t.integer "event_type"
    t.date    "event_date"
    t.integer "counter"
    t.integer "browser_id"
    t.integer "domain_id"
    t.integer "event_hour"
    t.index ["browser_id"], name: "index_events_on_browser_id", using: :btree
    t.index ["domain_id"], name: "index_events_on_domain_id", using: :btree
  end

end
