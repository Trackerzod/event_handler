class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.integer :event_type
      t.datetime :event_date
      t.integer :counter

      t.references :browser
      t.references :domain
    end
  end
end
