class CreateBrowsers < ActiveRecord::Migration[5.0]
  def change
    create_table :browsers do |t|
      t.string :name
    end
  end
end
