class AddEventHourToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :event_hour, :integer
  end
end
