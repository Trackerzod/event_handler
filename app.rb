require 'sinatra'
require 'sinatra/activerecord'
require './app/models/browser'
require './app/models/domain'
require './app/models/event'

get '/event.?:type?.?:referer?' do
  user_agent = request.user_agent
  user_domain = params[:referer].blank? ? request.env['REMOTE_ADDR'] : params[:referer]
  type = params[:type]

  if Browser.exists?(name: user_agent)
    browser = Browser.where(name: user_agent).first
  else
    browser = Browser.create(name: user_agent)
  end

  if Domain.exists?(name: user_domain)
    domain = Domain.where(name: user_domain).first
  else
    domain = Domain.create(name: user_domain)
  end

  if Event.exists?(event_type: type, domain_id: domain.id, browser_id: browser.id)
    updated_event = Event.where(event_type: type).where(domain_id: domain.id).where(browser_id: browser.id).first
    updated_event.counter += 1
    updated_event.event_date = Date.today
    updated_event.event_hour = Time.now.hour.to_i
    updated_event.save

  else
    Event.create(event_type: type, domain_id: domain.id, browser_id: browser.id, event_date: Date.today, event_hour: Time.now.hour.to_i, counter: 1)
  end
end